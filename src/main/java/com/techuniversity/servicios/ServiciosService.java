package com.techuniversity.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    static MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoDatabase database = mongoClient.getDatabase("dbprod");
        return database.getCollection("servicios");
    }

   /* public static void insert(String servicio) throws Exception {
        Document document = Document.parse(servicio);
        servicios = getServiciosCollection();
        servicios.insertOne(document);
    } */

    public static void insertBatch(String strServicios) throws Exception {
            servicios = getServiciosCollection();
            Document document = Document.parse(strServicios);
            List<Document> list = document.getList("servicios", Document.class);
            if(list == null){
                servicios.insertOne(document);
            } else {
                servicios.insertMany(list);
            }
    }

    public static List getAll() {
        servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> findIterable = servicios.find();
        Iterator it = findIterable.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static List getFiltrados(String filtro) {
        servicios = getServiciosCollection();
        List list = new ArrayList();
        Document document = Document.parse(filtro);
        FindIterable<Document> findIterable = servicios.find(document);
        Iterator it = findIterable.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static void update(String filtro, String servicio) {
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document document = Document.parse(servicio);
        servicios.updateOne(docFiltro, document);
    }
}
