package com.techuniversity.servicios;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class ServiciosController {

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServiciosService.getFiltrados(filtro);
    }

    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio) {
        try {
            ServiciosService.insertBatch(newServicio);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data) {
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ServiciosService.update(filtro,updates);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}